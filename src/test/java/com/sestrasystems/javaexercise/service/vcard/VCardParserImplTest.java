package com.sestrasystems.javaexercise.service.vcard;

import com.sestrasystems.javaexercise.domain.Contact;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class VCardParserImplTest {
    private VCardParserImpl parser;

    @Before
    public void setUp() throws Exception {
        parser = new VCardParserImpl();
    }

    @Test
    public void testEmptyVCardParsing() throws Exception {
        List<Contact> contacts = parser.parse(
                        "BEGIN:VCARD\n" +
                        "VERSION:2.1\n" +
                        "END:VCARD"
        );

        assertEquals(1, contacts.size());
    }

    @Test
    public void testOneVCardFNParsing() throws Exception {
        List<Contact> contacts = parser.parse(
                        "BEGIN:VCARD\n" +
                        "VERSION:2.1\n" +
                        "FN:Campus Police\n" +
                        "END:VCARD"
        );

        assertEquals(1, contacts.size());
        assertEquals("Campus Police", contacts.get(0).getFormattedName());
    }

    @Test
    public void testTwoVCardsFNParsing() throws Exception {
        List<Contact> contacts = parser.parse(
                        "BEGIN:VCARD\n" +
                        "VERSION:2.1\n" +
                        "FN:Campus Police\n" +
                        "END:VCARD\n" +

                        "BEGIN:VCARD\n" +
                        "VERSION:2.1\n" +
                        "FN:Ambulance\n" +
                        "END:VCARD"
        );

        assertEquals(2, contacts.size());
        assertEquals("Campus Police", contacts.get(0).getFormattedName());
        assertEquals("Ambulance", contacts.get(1).getFormattedName());
    }
}
