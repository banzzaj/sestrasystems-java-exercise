package com.sestrasystems.javaexercise.domain;

import java.util.Objects;

/**
 * Describes a credentials model
 */
public class Credentials {
    private final String login;
    private final String password;

    public Credentials(String login, String password) {
        Objects.requireNonNull(login, "Expected not null login");
        Objects.requireNonNull(password, "Expected not null password");

        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
}
