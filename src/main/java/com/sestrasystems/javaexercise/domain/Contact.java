package com.sestrasystems.javaexercise.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Describes a contact model
 */
public class Contact {
    @JsonProperty("formatted_name")
    private String formattedName;

    @JsonProperty("structured_name")
    private StructuredName structuredName;

    @JsonProperty("telephones")
    private List<Telephone> telephones;

    @JsonProperty("addresses")
    private List<Address> addresses;

    @JsonProperty("emails")
    private List<String> emails;

    @JsonProperty("revision")
    private String revision;

    @JsonProperty("default_postal_address")
    private String defaultPostalAddress;

    public String getFormattedName() {
        return formattedName;
    }

    public static class StructuredName {
        @JsonProperty("given")
        private String given;

        @JsonProperty("family")
        private String family;

        public void setGiven(String given) {
            this.given = given;
        }

        public void setFamily(String family) {
            this.family = family;
        }
    }

    public static class Telephone {
        @JsonProperty("text")
        private String text;

        @JsonProperty("types")
        private Set<String> types;

        public void setText(String text) {
            this.text = text;
        }

        public void addType(String type) {
            if (types == null) {
                types = new HashSet<>();
            }

            types.add(type);
        }
    }

    public static class Address {
        @JsonProperty("country")
        private String country;

        @JsonProperty("region")
        private String region;

        @JsonProperty("locality")
        private String locality;

        @JsonProperty("street_addr")
        private String streetAddress;

        @JsonProperty("postal_code")
        private String postalCode;

        @JsonProperty("types")
        private Set<String> types;

        public void setCountry(String country) {
            this.country = country;
        }

        public void setRegion(String region) {
            this.region = region;
        }

        public void setLocality(String locality) {
            this.locality = locality;
        }

        public void setStreetAddress(String streetAddress) {
            this.streetAddress = streetAddress;
        }

        public void setPostalCode(String postalCode) {
            this.postalCode = postalCode;
        }

        public void addType(String type) {
            if (types == null) {
                types = new HashSet<>();
            }

            types.add(type);
        }
    }

    public void setFormattedName(String formattedName) {
        this.formattedName = formattedName;
    }

    public void setStructuredName(StructuredName structuredName) {
        this.structuredName = structuredName;
    }

    public void addTelephone(Telephone telephone) {
        if (telephones == null) {
            telephones = new ArrayList<>();
        }

        telephones.add(telephone);
    }

    public void addAddress(Address address) {
        if (addresses == null) {
            addresses = new ArrayList<>();
        }

        addresses.add(address);
    }

    public void addEmail(String email) {
        if (emails == null) {
            emails = new ArrayList<>();
        }

        emails.add(email);
    }

    public void setRevision(String revision) {
        this.revision = revision;
    }

    public void setDefaultPostalAddress(String defaultPostalAddress) {
        this.defaultPostalAddress = defaultPostalAddress;
    }
}
