package com.sestrasystems.javaexercise.service;

import com.sestrasystems.javaexercise.domain.Contact;
import com.sestrasystems.javaexercise.service.http.HttpStatusCodes;
import com.sestrasystems.javaexercise.service.vcard.VCardParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Objects;

/**
 * Provides contacts that retrieved from dev.dublabs.com
 */
public class ContactsServiceImpl implements ContactsService {
    private static final Logger LOG = LoggerFactory.getLogger(ContactsServiceImpl.class);
    private static final String EMERGENCY_CONTACTS_URL = "http://dev.dublabs.com:8000/mobileCampus/json/emergencyContacts";

    private final VCardParser vCardParser;

    public ContactsServiceImpl(VCardParser vCardParser) {
        Objects.requireNonNull(vCardParser, "Expected not null vCardParser");
        this.vCardParser = vCardParser;
    }

    @Override
    public List<Contact> getEmergencyContacts() {
        String contactsContent = getEmergencyContactsContent();
        return vCardParser.parse(contactsContent);
    }

    private String getEmergencyContactsContent() {
        try {
            URL url = new URL(EMERGENCY_CONTACTS_URL);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");

            LOG.debug("Retrieve data from {}", EMERGENCY_CONTACTS_URL);

            int responseCode = con.getResponseCode();
            LOG.debug("Response code: {}", responseCode);

            if (responseCode != HttpStatusCodes.OK) {
                throw new RuntimeException("Could not get content from " + EMERGENCY_CONTACTS_URL);
            }

            try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine + "\n");
                }

                String content = response.toString();
                LOG.debug("Content: " + content);
                return content;
            }
        } catch (Exception e) {
            LOG.error("Failed to get emergency contacts: {}", e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }
}
