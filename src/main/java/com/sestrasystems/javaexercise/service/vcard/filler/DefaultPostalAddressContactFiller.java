package com.sestrasystems.javaexercise.service.vcard.filler;

import com.sestrasystems.javaexercise.domain.Contact;
import ezvcard.VCard;
import ezvcard.property.RawProperty;

public class DefaultPostalAddressContactFiller implements ContactFiller {
    @Override
    public void fill(Contact contact, VCard vCard) {
        RawProperty extendedProperty = vCard.getExtendedProperty("X-MS-OL-DEFAULT-POSTAL-ADDRESS");

        if (extendedProperty != null) {
            contact.setDefaultPostalAddress(extendedProperty.getValue());
        }
    }
}
