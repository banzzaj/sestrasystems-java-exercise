package com.sestrasystems.javaexercise.service.vcard;

import com.sestrasystems.javaexercise.domain.Contact;
import com.sestrasystems.javaexercise.service.vcard.filler.FullContactFiller;
import ezvcard.Ezvcard;
import ezvcard.VCard;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Parses VCard content using the ez-vcard library (https://github.com/mangstadt/ez-vcard)
 */
public class VCardParserImpl implements VCardParser {
    private final FullContactFiller fullContactFiller = new FullContactFiller();

    @Override
    public List<Contact> parse(String content) {
        return Ezvcard.parse(content).all().stream()
                .map(this::vCardToContact)
                .collect(Collectors.toList());
    }

    private Contact vCardToContact(VCard vCard) {
        Contact contact = new Contact();
        fullContactFiller.fill(contact, vCard);
        return contact;
    }
}
