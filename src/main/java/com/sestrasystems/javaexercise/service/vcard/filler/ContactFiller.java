package com.sestrasystems.javaexercise.service.vcard.filler;

import com.sestrasystems.javaexercise.domain.Contact;
import ezvcard.VCard;

/**
 * Fills some {@code Contact} fields
 */
public interface ContactFiller {
    void fill(Contact contact, VCard vCard);
}
