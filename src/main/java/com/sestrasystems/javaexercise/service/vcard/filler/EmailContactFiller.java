package com.sestrasystems.javaexercise.service.vcard.filler;

import com.sestrasystems.javaexercise.domain.Contact;
import ezvcard.VCard;

public class EmailContactFiller implements ContactFiller {
    @Override
    public void fill(Contact contact, VCard vCard) {
        vCard.getEmails().forEach(e -> {
            contact.addEmail(e.getValue());
        });
    }
}
