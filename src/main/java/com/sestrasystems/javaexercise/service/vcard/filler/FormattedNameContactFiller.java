package com.sestrasystems.javaexercise.service.vcard.filler;

import com.sestrasystems.javaexercise.domain.Contact;
import ezvcard.VCard;
import ezvcard.property.FormattedName;

public class FormattedNameContactFiller implements ContactFiller {
    @Override
    public void fill(Contact contact, VCard vCard) {
        FormattedName formattedName = vCard.getFormattedName();
        if (formattedName != null) {
            contact.setFormattedName(formattedName.getValue());
        }
    }
}
