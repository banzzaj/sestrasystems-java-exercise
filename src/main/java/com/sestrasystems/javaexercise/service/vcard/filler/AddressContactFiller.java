package com.sestrasystems.javaexercise.service.vcard.filler;

import com.sestrasystems.javaexercise.domain.Contact;
import ezvcard.VCard;

public class AddressContactFiller implements ContactFiller {

    @Override
    public void fill(Contact contact, VCard vCard) {
        vCard.getAddresses().forEach(addr -> {
            Contact.Address address = new Contact.Address();

            address.setCountry(addr.getCountry());
            address.setRegion(addr.getRegion());
            address.setLocality(addr.getLocality());
            address.setPostalCode(addr.getPostalCode());
            addr.getTypes().forEach(t -> {
                address.addType(t.getValue());
            });

            contact.addAddress(address);
        });
    }
}
