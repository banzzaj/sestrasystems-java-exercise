package com.sestrasystems.javaexercise.service.vcard.filler;

import com.sestrasystems.javaexercise.domain.Contact;
import ezvcard.VCard;

public class TelephonesContactFiller implements ContactFiller {
    @Override
    public void fill(Contact contact, VCard vCard) {
        vCard.getTelephoneNumbers().forEach(t -> {
            Contact.Telephone telephone = new Contact.Telephone();

            telephone.setText(t.getText());
            t.getTypes().forEach(type -> telephone.addType(type.getValue()));

            contact.addTelephone(telephone);
        });
    }
}
