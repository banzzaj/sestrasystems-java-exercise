package com.sestrasystems.javaexercise.service.vcard.filler;

import com.sestrasystems.javaexercise.domain.Contact;
import ezvcard.VCard;

import java.util.Arrays;
import java.util.List;

public class FullContactFiller implements ContactFiller {

    private final List<ContactFiller> contactFillers = Arrays.asList(
            new FormattedNameContactFiller(),
            new StructuredNameContactFiller(),
            new TelephonesContactFiller(),
            new AddressContactFiller(),
            new EmailContactFiller(),
            new RevisionContactFiller(),
            new DefaultPostalAddressContactFiller()
    );

    @Override
    public void fill(Contact contact, VCard vCard) {
        contactFillers.forEach(filler -> filler.fill(contact, vCard));
    }
}
