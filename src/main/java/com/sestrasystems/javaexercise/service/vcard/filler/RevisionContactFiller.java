package com.sestrasystems.javaexercise.service.vcard.filler;

import com.sestrasystems.javaexercise.domain.Contact;
import ezvcard.VCard;
import ezvcard.property.Revision;

import java.text.SimpleDateFormat;

public class RevisionContactFiller implements ContactFiller {
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY-MM-DD hh:mmZ");

    @Override
    public void fill(Contact contact, VCard vCard) {
        Revision vCardRevision = vCard.getRevision();
        if (vCardRevision != null) {
            contact.setRevision(dateFormat.format(vCardRevision.getValue()));
        }
    }
}
