package com.sestrasystems.javaexercise.service.vcard;

import com.sestrasystems.javaexercise.domain.Contact;

import java.util.List;

/**
 * The parser allows to parse VCard string content into a list of {@code Contact} pojo's
 */
public interface VCardParser {

    /**
     * Parses the given VCard string into a list of {@code Contact} pojo's
     * @param content content to parse
     * @return a list of parsed contacts
     */
    List<Contact> parse(String content);
}
