package com.sestrasystems.javaexercise.service.vcard.filler;

import com.sestrasystems.javaexercise.domain.Contact;
import ezvcard.VCard;
import ezvcard.property.StructuredName;

public class StructuredNameContactFiller implements ContactFiller {
    @Override
    public void fill(Contact contact, VCard vCard) {
        StructuredName vCardStructuredName = vCard.getStructuredName();

        if (vCardStructuredName != null) {
            Contact.StructuredName structuredName = new Contact.StructuredName();
            structuredName.setGiven(vCardStructuredName.getGiven());
            structuredName.setFamily(vCardStructuredName.getFamily());
            contact.setStructuredName(structuredName);
        }
    }
}
