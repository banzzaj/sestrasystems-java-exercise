package com.sestrasystems.javaexercise.service;

import com.sestrasystems.javaexercise.domain.Contact;

import java.util.List;

/**
 * Provides contacts
 */
public interface ContactsService {

    /**
     * Returns emergency contacts
     * @return a list of emergency contacts
     */
    List<Contact> getEmergencyContacts();
}
