package com.sestrasystems.javaexercise.service.http;

/**
 * Contains the standard HTTP headers
 */
public class HttpHeaders {
    public static final String AUTHORIZATION = "Authorization";
    public static final String CONTENT_TYPE = "Content-Type";
}
