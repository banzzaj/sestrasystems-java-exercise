package com.sestrasystems.javaexercise.service.http;

/**
 * Contains the standard HTTP status codes
 */
public class HttpStatusCodes {
    public static final int OK = 200;
    public static final int UNAUTHORIZED = 401;
}
