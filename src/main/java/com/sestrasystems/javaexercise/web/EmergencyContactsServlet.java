package com.sestrasystems.javaexercise.web;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sestrasystems.javaexercise.domain.Contact;
import com.sestrasystems.javaexercise.domain.Credentials;
import com.sestrasystems.javaexercise.service.ContactsService;
import com.sestrasystems.javaexercise.service.ContactsServiceImpl;
import com.sestrasystems.javaexercise.service.http.HttpHeaders;
import com.sestrasystems.javaexercise.service.vcard.VCardParserImpl;
import com.sestrasystems.javaexercise.web.util.AuthenticationUtil;
import com.sestrasystems.javaexercise.service.http.HttpStatusCodes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * The servlet provides emergency contacts
 */
public class EmergencyContactsServlet extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(EmergencyContactsServlet.class);

    private final ContactsService contactsService = new ContactsServiceImpl(new VCardParserImpl());
    private final ObjectMapper objectMapper = new ObjectMapper() {{
        setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }};

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Credentials credentials = AuthenticationUtil.credentialsWithBasicAuthentication(req);

        if (!isAuthorized(credentials)) {
            resp.setStatus(HttpStatusCodes.UNAUTHORIZED);
            resp.getOutputStream().write("Unauthorized".getBytes());
            return;
        }
        printCredentials(credentials);

        List<Contact> contacts = contactsService.getEmergencyContacts();
        String contactsJson = toJson(contacts);
        printContacts(contactsJson);

        resp.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        resp.getOutputStream().write(contactsJson.getBytes());
    }

    private boolean isAuthorized(Credentials credentials) {
        return credentials != null;
    }

    private void printCredentials(Credentials credentials) {
        LOG.info("User credentials: user={}, password={}", credentials.getLogin(), credentials.getPassword());
    }

    private String toJson(Object o) {
        try {
            return objectMapper.writeValueAsString(o);
        } catch (JsonProcessingException e) {
            final String errMsg = "Could not write result as json";
            LOG.error(errMsg, e);
            throw new RuntimeException(errMsg, e);
        }
    }

    private void printContacts(String contactsJson) {
        LOG.info("Contacts: {}", contactsJson);
    }
}
