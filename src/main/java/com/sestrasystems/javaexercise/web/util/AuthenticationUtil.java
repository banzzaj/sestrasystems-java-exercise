package com.sestrasystems.javaexercise.web.util;

import com.sestrasystems.javaexercise.service.http.HttpHeaders;
import com.sestrasystems.javaexercise.domain.Credentials;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.util.Base64;

public class AuthenticationUtil {

    /**
     * Gets credentials that got from the given request using basic authentication method
     * @param req a given request
     * @return credentials if basic authentication is present and valid, or null otherwise
     */
    public static Credentials credentialsWithBasicAuthentication(HttpServletRequest req) {
        String authHeader = req.getHeader(HttpHeaders.AUTHORIZATION);

        if (authHeader == null) {
            return null;
        }

        String[] tokens = authHeader.split(" ");
        if (tokens.length != 2) {
            return null;
        }

        if (!tokens[0].equalsIgnoreCase("Basic")) {
            return null;
        }

        try {
            String credentials = new String(Base64.getDecoder().decode(tokens[1]), "UTF-8");
            int index = credentials.indexOf(":");
            if (index != -1) {
                String login = credentials.substring(0, index).trim();
                String password = credentials.substring(index + 1).trim();

                return new Credentials(login, password);
            } else {
                return null;
            }
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Could not retrieve authentication");
        }
    }
}
