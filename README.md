Sestrasystems java exercise
===========================

A Java servlet that implements java server programming exercise. The full exercise description is in the file `doc/Exercise-JavaServerProgramming.pdf`.

The servlet implements one endpoint:

- method: GET
- url: /
 
How to build
------------

The project uses `gradle` as a build tool.
To build the project execute one of the commands below

for unix based systems:
```
./gradlew clean build
```

for windows based systems:
```
./gradlew.bat clean build
```

The build output directory `build/libs` will contain the built war file `sestrasystems-java-exercise.war`.

How to run locally
------------------

The gradle Jetty plugin allows you to run the servlet locally without an installed application server.
To do this execute one of the commands below

for unix based systems:
```
./gradlew jettyRunWar
```

for windows based systems:
```
./gradlew.bat jettyRunWar
```

Jetty will be ran on the port that is set in `gradle.build`

You can check the servlet works by using `httpie` or another tool that provides sending HTTP requests.
For example:
```
http --auth user:pwd --auth-type basic --pretty all GET http://localhost:8088/sestrasystems-java-exercise/
```